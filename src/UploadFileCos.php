<?php
/*
 * @Author: kaleozhou
 * @Email: kaleo1990@hotmail.com
 * @Blog: www.kaleozhou.top
 * @Date: 2021-05-20 22:06:14
 * @LastEditors: kaleozhou
 * @LastEditTime: 2021-05-21 07:54:16
 * @Description: content
 */

namespace Encore\UploadFileCos;

use Encore\Admin\Extension;

class UploadFileCos extends Extension
{
    public $name = 'uploadfilecos';

    public $views = __DIR__.'/../resources/views';

    public $assets = __DIR__.'/../resources/assets';

    // public $menu = [
    //     'title' => 'Uploadfilecos',
    //     'path'  => 'uploadfilecos',
    //     'icon'  => 'fa-gears',
    // ];
}