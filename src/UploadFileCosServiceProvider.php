<?php
/*
 * @Author: kaleozhou
 * @Email: kaleo1990@hotmail.com
 * @Blog: www.kaleozhou.top
 * @Date: 2021-05-20 22:06:14
 * @LastEditors: kaleozhou
 * @LastEditTime: 2021-05-21 08:48:48
 * @Description: content
 */

namespace Encore\UploadFileCos;

use Illuminate\Support\ServiceProvider;
use Encore\Admin\Admin;

class UploadFileCosServiceProvider extends ServiceProvider
{
    /**
     * {@inheritdoc}
     */
    public function boot(UploadFileCos $extension)
    {
        if (! UploadFileCos::boot()) {
            return ;
        }

        if ($views = $extension->views()) {
            $this->loadViewsFrom($views, 'uploadfilecos');
        }

        if ($this->app->runningInConsole() && $assets = $extension->assets()) {
            $this->publishes(
                [$assets => public_path('vendor/laravel-admin-ext/uploadfilecos')],
                'uploadfilecos'
            );
        }
        Admin::booted(function(){
            Admin::js('/vendor/laravel-admin-ext/uploadfilecos/cos-auth.min.js');
            Admin::js('/vendor/laravel-admin-ext/uploadfilecos/bootstrap.file-input.js');
        });
        $this->app->booted(function () {
            UploadFileCos::routes(__DIR__.'/../routes/web.php');
        });
    }
}