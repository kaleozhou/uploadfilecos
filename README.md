# uploadfilecos

1. 在filesystems.php下添加一下配置，并在.env中配置相关环境变量（之所以用这个配置，是因为我用了另外一个包 "freyo/flysystem-qcloud-cos-v5": "^2.2")，不愿意单独也一份，就直接用了。

   ```php
     'cosv5' => [
               'driver' => 'cosv5',
               'region'          => env('COSV5_REGION', 'ap-chongqing'),
               'credentials'     => [
                   'appId'     => env('COSV5_APP_ID'),
                   'secretId'  => env('COSV5_SECRET_ID'),
                   'secretKey' => env('COSV5_SECRET_KEY'),
               ],
               'timeout'         => env('COSV5_TIMEOUT', 60),
               'connect_timeout' => env('COSV5_CONNECT_TIMEOUT', 60),
               'bucket'          => env('COSV5_BUCKET'),
               'cdn'             => env('COSV5_CDN'),
               'scheme'          => env('COSV5_SCHEME', 'https'),
               'read_from_cdn'   => env('COSV5_READ_FROM_CDN', false),
         ],
   
   ```

   

2. 安装本扩展

```php
composer require kaleozhou/uploadfilecos
```

3. 发布资源

```php
php artisan vendor:publish --tag=uploadfilecos
```

4. 在app/Admin/bootstrap.php中注册

```php
Encore\Admin\Form::extend('uploadfilecos',\Encore\UploadFileCos\UploadFileCosField::class);
```

5. 使用

group分组
mutiple
true:多文件
false:单文件
accept:文件类型
```php
 $form->uploadfilecos('url', __('Video'))->group('videos')->multiple('ture')->accept('.mp4');
//group 分组
//mutiple true:多文件 false:单文件
```

