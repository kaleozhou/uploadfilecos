<div class="form-group {!! !$errors->has($label) ?: 'has-error' !!}">
    <label for="{{$id}}" class="col-sm-2 control-label">{{$label}}</label>
    <div class="col-sm-6">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        @include('admin::form.error')
        <div class="controls" id="uploadfilecos_wrapper-{{$name}}">
            <input type="file" id="{{$name}}-resource" data-filename-placement="inside" placeholder="选择文件"
                class="file-inputs" />
            <small class="ml-5" id='{{$name}}-hint' hidden> 一次上传一个文件，多次点击上传多个文件 </small>
            <div class="progress " style="height: 10px;margin-bottom: 2px;margin-top: 10px;width: 200px;">
                <div id="{{$name}}-progressbar" style="background:lightseagreen;height:10px;width:0;"></div>
            </div>
            <ul class="list-unstyled">
                @if (is_array(old($column, $value)))
                @foreach (old($column, $value) as $key=>$item)
                <li>
                    <a class='{{$name}}-deleteFile' value='{{$item}}'><span
                            class="text-black">{{getFileName($item)}}</span> <i class="fa fa-trash">删除</i></a>
                </li>
                @endforeach
                @else
                <li>
                    <a class='{{$name}}-deleteFile' value='{{old($column, $value)}}'><span
                            class="text-black">{{getFileName(old($column, $value))}}</span> <i
                            class="fa fa-trash">删除</i></a>
                </li>
                @endif
                <li style="font-size:12px;color:#aaa;" id="{{$name}}-output"></li>
            </ul>

            <input type="hidden" name="{{$name}}" id="{{$name}}-savedpath"
                value="{{is_array(old($column, $value))?implode(',',old($column, $value)):old($column, $value) }}">
        </div>
        @include('admin::form.help-block')
    </div>
</div>